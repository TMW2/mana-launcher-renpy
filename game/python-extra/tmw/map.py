#!/usr/bin/env python3
from .packets_in import in_packets
from .packets_out import out_packets


class Map(object):
    def __init__(self, socket, debugging=False):
        self.socket = socket
        self.debugging = debugging

# placeholder
