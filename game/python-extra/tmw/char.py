#!/usr/bin/env python3

import struct
from .packets_in import in_packets
from .packets_out import out_packets


class Char(object):
    """
        Author: jak1
        License: GPLv2+
        State: Testing (Draft)
        Class Char:
            handles Char Packets (some packets can only get used in the right order!)
            socket: bound char server socket
            debugging: enabled if True, disabled otherwise

            NOTE: does the stored data allready have the right type? may i need to convert/cast them.
    """

    def __init__(self, socket, debugging=False):
        self.socket = socket
        self.debugging = debugging

    def sendLogin(self, char_logindata, client_protocol_version):
        """
            Function: sendLogin
                send login data to char server
                char_logindata: (dict) (of Login.getCharLoginData)
                returns: None
        """
        out_packets[0x0065].send_data(
            self.socket, [char_logindata['account_id'], char_logindata['session_id_p1'], char_logindata['session_id_p2'], client_protocol_version, char_logindata['gender']], self.debugging)
        out_packets[0x0065].process_data()

    def getLoginChars(self):
        """
            Function: getLoginChars
                recvs character-slots
                returns: TODO: desc-> [chars(...)]
        """
        # FIXME: can hit the size, better using 2048 (breaking or lagging), while logging in it
        #        should not matter to have lag, so i guess its better save then sorry
        in_packets[0x006b].recv_data(self.socket, 1024, self.debugging)
        in_packets[0x006b].process_data()

        char_packet_len = 144
        chars_raw = in_packets[0x006b].get_list_data()[10]
        char_count = int(len(chars_raw) / char_packet_len)

        # slot[%] = {char_data}
        slots = [{}]

        for x in range(char_count):

            GID, exp, money, jobexp, joblevel, bodystate, healthstate, effectstate, virtue, honor, jobpoint, hp, \
                maxhp, sp, maxsp, speed, job, head, weapon, level, sppoint, accessory, shield, accessory2, accessory3, \
                headpalette, bodypalette, name, Str, Agi, Vit, Int, Dex, Luk, CharNum, haircolor, bIsChangedCharName, \
                lastMap, DeleteDate, Robe, SlotAddon, RenameAddon = struct.unpack(
                    "<L I I I I I I I I I H I I H H H H H H H H H H H H H H 24s s s s s s s s s H 16s I I I I", chars_raw[(x * char_packet_len):(x * char_packet_len) + char_packet_len])
            slots[x] = {
                "GID": GID,
                "exp": exp,
                "money": money,
                "jobexp": jobexp,
                "joblevel": joblevel,
                "bodystate": bodystate,
                "healthstate": healthstate,
                "effectstate": effectstate,
                "virtue": virtue,
                "honor": honor,
                "jobpoint": jobpoint,
                "hp": hp,
                "maxhp": maxhp,
                "sp": sp,
                "maxsp": maxsp,
                "speed": speed,
                "job": job,
                "head": head,
                "weapon": weapon,
                "level": level,
                "sppoint": sppoint,
                "accessory": accessory,
                "shield": shield,
                "accessory2": accessory2,
                "accessory3": accessory3,
                "headpalette": headpalette,

                "bodypalette": bodypalette,
                "name": name.decode('ascii'),
                "Str": int.from_bytes(Str, 'little'),
                "Agi": int.from_bytes(Agi, 'little'),
                "Vit": int.from_bytes(Vit, 'little'),
                "Int": int.from_bytes(Int, 'little'),
                "Dex": int.from_bytes(Dex, 'little'),
                "Luk": int.from_bytes(Luk, 'little'),

                "CharNum": int.from_bytes(CharNum, 'little'),
                "haircolor": int.from_bytes(haircolor, 'little'),
                "bIsChangedCharName": bIsChangedCharName,

                "lastMap": lastMap.decode('ascii'),
                "DeleteDate": DeleteDate,
                "Robe": Robe,
                "SlotAddon": SlotAddon,
                "RenameAddon": RenameAddon
            }
            continue
        # FIXME: adding header values? do we need it? beside the "time created" maybe
        return (char_count, slots)

    def selectChar(self, slot):
        """
            Function: selectChar
                select a character using the slot id (0 is the first)
                slot: character slot id (int)
                returns: None
        """
        out_packets[0x0066].send_data(
            self.socket, [slot], self.debugging)
        out_packets[0x0066].process_data()

    def getMapInfo(self):
        """
            Function: getMapInfo
                recv information about the map server
                returns: tuple(char_id(int), map_name(str), host(str), port(int))
        """
        in_packets[0x0ac5].recv_data(self.socket, 28, self.debugging)
        in_packets[0x0ac5].process_data()
        _, char_id, map_name, host, port = in_packets[0x0ac5].get_list_data()
        host = "{}.{}.{}.{}".format(*(host))
        return (char_id, map_name, host, port)

    def skipPacket(self, packet_id, size=1024):
        if in_packets[packet_id].p_len != -1:
            size = in_packets[packet_id].p_len
        in_packets[packet_id].recv_data(self.socket, size, self.debugging)
        return None
