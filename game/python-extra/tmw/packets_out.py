#!/usr/bin/env python3
from .packets import Packet

"""
    Packet dict
    p_id also known as packet_type
    : Packet(
        p_name:         PACKET_NAME 
        p_id:           FIXME: can i get th own id inside the packet, so i dont need them twice?
        p_struct:       [byte structure] @see pattern in packets.py
        p_len:          packet size
        p_response:     response p_id
        p_skip:         skips the whole packet
    )
    returns: None
"""
out_packets = {
    # CMSG_%
    # HPM: https://gitlab.com/evol/evol-hercules/-/blob/master/src/elogin/init.c#L45
    0x7530: Packet("SERVER_VERSION_REQUEST", 0x7530, ['H', 'b', '19x'], 22),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/login/packets_ca_struct.h#L52 FIXME: encryption, hashing?!
    0x0064: Packet("LOGIN_REGISTER",         0x0064, ['H', 'I', '24s', '24s', 'b'], 55),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/char/char.c#L5223
    0x0065: Packet("CHAR_SERVER_CONNECT",    0x0065, ['H', 'I', 'I', 'I', 'H', 'b'], 17),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/char/char.c#L5233
    0x0066: Packet("CHAR_SELECT",            0x0066, ['H', 'b'], 3),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/map/packets.h#L1641
    0x089c: Packet("MAP_SERVER_CONNECT",     0x089c, ['H', 'I', 'I', 'I', 'I', 'b'], 19),
    # NOTE: HERC: https://gitlab.com/evol/hercules/-/blob/master/src/map/clif.c#L10625
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/map/packets.h#L40
    0x007e: Packet("MAP_PING",               0x007e, ['H'], 2),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/map/packets.h#L39
    0x007d: Packet("MAP_LOADED",             0x007d, ['H'], 2),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/char/char.c#L5274 FIXME: len 6? why do i have a len of 56? (maybe appending packets?)
    0x0187: Packet("CHAR_PING",              0x0187, [], 56),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/map/packets.h#L130
    0x018a: Packet("CLIENT_QUIT",            0x018a, [], 4),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/map/packets.h#L361
    0x00f3: Packet("CHAT_MESSAGE",           0x00f3, ['H', 'H', '???'], -1),

}

if __name__ == "__main__":
    print("this is a module, and can't be used as regular main file!")
    exit(1)
