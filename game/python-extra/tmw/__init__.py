# TMW Plugin, both built-in client as Google Auth workaround
import socket, traceback

from .login import Login
from .char import Char

NETLIB_VERSION = 1

running = False

# sockets dict
sockets = {
    'login': socket.socket(socket.AF_INET, socket.SOCK_STREAM),
    'char': socket.socket(socket.AF_INET, socket.SOCK_STREAM),
    'map': socket.socket(socket.AF_INET, socket.SOCK_STREAM)
}

def client(OPT, PWD):
    print("---- I AM BEING RUN")
    print("++++ OPT %s" % OPT)
    try:
        options = OPT.split(" ")
        bf=""
        HOST=""
        PORT=""
        # "-s %s -y evol2 -p %s -S" % (HOST, PORT)
        for op in options:
            # Defines (we don't care for -y or -S)
            if op == "-s":
                bf="HOST"
            elif op == "-p":
                bf="PORT"
            # Values
            elif bf == "HOST":
                HOST=str(op)
                bf=""
            elif bf == "PORT":
                PORT=str(op)
                bf=""

        PORT = int(PORT)
        running = True

        # FIXME - resolve HOST to an IP?
        status = loop(running, HOST, PORT)
        print("---- RESULT %d" % status)
        return status
    except:
        traceback.print_exc()
        return -1


def loop(state, login_ip, login_port):
    """
        main network loop
    """
    active_server = 1
    print("Looping while %d" % state)

    while state:
        try:
            if((sockets['login'] is not None) and
               (sockets['char'] is not None) and
               (sockets['map'] is not None)):
                if active_server == 1:
                    # login
                    print("LoginServer")
                    sockets['login'].connect((login_ip, login_port))
                    sockets['login'].setblocking(1)  # important!
                    login = Login(sockets['login'], True)
                    login.sendServerVersionRequest()
                    login.getUpdateHost()
                    login.sendLoginRegister('testacc', 'testacc')
                    # catch login errors if any?
                    # (may i need to change the packet handling then, caching)
                    _char = login.getCharLoginData()
                    sockets['login'].close()
                    active_server += 1
                elif active_server == 2:
                    # char server
                    print("CharServer")
                    # TODO: choose server (just first one rn.)
                    sockets['char'].connect(
                        (_char['char_server'][0]['host'], _char['char_server'][0]['port']))
                    sockets['char'].setblocking(1)  # important!
                    char = Char(sockets['char'], True)
                    char.sendLogin(_char, 26)
                    char.skipPacket(0x8481)
                    char.skipPacket(0x082d)
                    char.getLoginChars()
                    # TODO: choose character (just first one rn.)
                    char.selectChar(0)
                    char.getMapInfo()
                    active_server += 1
                elif active_server == 3:
                    # map server
                    print("MapServer")
                    # currently just exit, debugging doesnt work while getting floaded with prints
                    return 0
                else:
                    # this should not happen, but why not
                    return -1

            else:
                print("SOCKET ERROR")
                raise Exception("socket bind error").with_traceback(None)
        except socket.error:
            print('ERROR: Bind failed.')
            return -2
        except KeyboardInterrupt:
            # makes 'ctrl + c' work again in a while loop,
            # so we can kill all sockets when they arnt death allrdy.
            print("INFO: exited")
            if sockets['map'] is not None:
                sockets['map'].close()
            if sockets['char'] is not None:
                sockets['char'].close()
            if sockets['login'] is not None:
                sockets['login'].close()
            return 0

    print("INFO: Aborted.")
    return 0
