#!/usr/bin/env python3
from .packets import Packet

"""
    Packet dict
    p_id also known as packet_type
    : Packet(
        p_name:         PACKET_NAME 
        p_id:           FIXME: can i get th own id inside the packet, so i dont need them twice?
        p_struct:       [byte structure] @see pattern in packets.py
        p_len:          packet size
        p_response:     response p_id
        p_skip:         skips the whole packet
    )
    returns: None
"""
in_packets = {
    # SMSG_%
    # HPM: https://gitlab.com/evol/evol-hercules/-/blob/master/src/elogin/init.c#L49
    0x7531: Packet("SERVER_VERSION_RESPONSE", 0x7531, ['H', 'H', 'L', 'L', 'L', '*s'], -1),
    # HPM: https://gitlab.com/evol/evol-hercules/-/blob/master/src/elogin/init.c#L50
    0x0063: Packet("UPDATE_HOST",            0x0063, ['H', 'H', '*s'], -1),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/login/packets_ac_struct.h#L72
    0x0ac4: Packet("LOGIN_DATA",             0x0ac4, ['H', 'H', 'I', 'I', 'I', 'I', '24s', 'H', 'b', '17s', '*s'], -1),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/char/char.c#L2229
    0x006b: Packet("CHAR_LOGIN",             0x006b, ['H', 'H', 's', 's', 's', '4s', 'L', 'L', 'L', '7s', '*s'], -1),
    0x082d: Packet("CHAR_LOGIN2",            0x082d, [], 29),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/char/char.c#L4574 XXX: 28/156 ?!
    0x0ac5: Packet("CHAR_MAP_INFO",          0x0ac5, ['H', 'I', '16s', '4s', 'H'], 28),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/map/clif.c#L10603
    0x006a: Packet("LOGIN_ERROR",            0x006a, ['H', 'b', '20s'], 23),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/login/lclif.c#L330
    0x0b02: Packet("LOGIN_ERROR2",           0x0b02, ['H', 'L' '20s'], 26),
    # NOTE: WHY are there 3 packets for error handling on the login server o_O
    0x0081: Packet("LOGIN_CONNECTION_ERROR3", 0x0081, ['H', 'b'], 3),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/char/char.c#L2429
    0x006c: Packet("CHAR_LOGIN_ERROR",       0x006c, ['H', 'b'], 3),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/map/clif.c#L723
    0x02eb: Packet("MAP_LOGIN_SUCCESS",      0x02eb, ['H', 'L', '3s', 's', 's', 'H'], 13),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/map/clif.c#L10687 # XXX: ignored packet
    0x0283: Packet("MAP_ACCOUNT_ID",         0x0283, ['H', 'L'], 6, p_skip=True),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/map/clif.c#L2017
    0x0091: Packet("PLAYER_WARP",            0x0091, ['H', '16s', 'H', 'H'], 22),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/map/packets_struct.h#L3275
    0x0b1d: Packet("MAP_PING2",              0x0b1d, ['H'], 2),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/map/clif.c#L11909 FIXME: len 5 or 7?
    0x0437: Packet("PLAYER_CHANGE_ACT",      0x0437, [], 7),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/map/clif.c#L6117
    0x043f: Packet("BEING_STATUS_CHANGE",    0x043f, [], 25),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/map/clif.c#L11248
    0x007f: Packet("SERVER_PING",            0x007f, ['H', 'L'], 6),
    # HERC: https://gitlab.com/evol/hercules/-/blob/master/src/map/clif.c#L886
    0x0080: Packet("BEING_REMOVE",           0x0080, ['H', 'L', 'b'], 7),
    # HERC/M+: 'Dull' ?
    0x0894: Packet("IGNORED",                0x0894, [], -1, p_skip=True),
    0x8481: Packet("IGNORED",                0x8481, [], 4, p_skip=True),
    # M+: https://gitlab.com/manaplus/manaplus/-/blob/master/src/net/eathena/mail2recv.cpp#L329 # NOTE: has subpacket ((len(packet)-6?)/43?)
    0x0ac2: Packet("MAIL2_MAIL_LIST_PAGE",   0x0ac2, ['H', 'H', 'b', 'b', '43s'], -1)

}

if __name__ == "__main__":
    print("this is a module, and can't be used as regular main file!")
    exit(1)
