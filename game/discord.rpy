#################################################################################
#     This file is part of Mana Launcher.
#     Copyright (C) 2021  Jesusalva <jesusalva@tmw2.org>
#
#     Credits: https://arianeb.com/2019/07/19/adding-discord-rich-presence-to-renpy-games/
#     Note: Likely no license applicable.
#################################################################################

init -2 python:
    def readyCallback(current_user):
        print('Our user: {}'.format(current_user))

    def disconnectedCallback(codeno, codemsg):
        print('Disconnected from Discord rich presence RPC. Code {}: {}'.format(
            codeno, codemsg
        ))

    def errorCallback(errno, errmsg):
        print('An error occurred! Error {}: {}'.format(
            errno, errmsg
        ))

    def RPCUpdate(title, image):
        if not persistent.discord:
            stdout("Discord RPC disabled, update skipped")
            return
        discord_rpc.update_connection()
        discord_rpc.run_callbacks()
        discord_rpc.update_presence(
            **{
                'details': title,
                'start_timestamp': float(now()),
                'large_image_key': image
            }
        )
        #        'state': subtitle,
        discord_rpc.update_connection()
        discord_rpc.run_callbacks()

label before_main_menu:
    if not persistent.discord:
        $ stdout("Discord RPC disabled")
        return
    python:
        # Note: 'event_name': callback
        callbacks = {
            'ready': readyCallback,
            'disconnected': disconnectedCallback,
            'error': errorCallback,
        }
        discord_rpc.initialize('840427221193195541', callbacks=callbacks, log=False)
        start = time.time()
        print(start)
        discord_rpc.update_connection()
        discord_rpc.run_callbacks()
        discord_rpc.update_presence(
            **{
                'details': 'Main Menu',
                'start_timestamp': float(now()),
                'large_image_key': 'launcher'
            }
        )
        discord_rpc.update_connection()
        discord_rpc.run_callbacks()
    return

label quit:
    $ responsive=False
    $ print("")
    python:
        try:
            stdout("Shutdown requested, cleaning up...")
            if persistent.discord:
                discord_rpc.shutdown()
            stdout("Thanks for playing the Mana Launcher.")
        except:
            pass
    return

