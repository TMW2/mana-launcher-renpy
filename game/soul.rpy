#################################################################################
#     This file is part of Mana Launcher.
#     Copyright (C) 2021  Jesusalva <jesusalva@tmw2.org>
#
#     Distributed under the MIT license, except for Steam parts.
#################################################################################
screen souldata():
    zorder 100
    if mySoul is not None:
      fixed:
        frame:
            background Frame("images/default.png", 0, 0)
            xalign 0.5
            yalign 0.5
            xminimum 0.85
            xmaximum 0.85
            yminimum 0.8
            ymaximum 0.8
            hbox:
                null width 320
                null width 40
                vbox:
                    null height 20
                    label "{b}{color=#FFF}%s{/color}{/b}" % mySoul["name"]
                    null height 20
                    hbox:
                        label "{color=#FFF}0{/color}"
                        null width 20
                        bar:
                            value mySoul["exp"]
                            range mySoul["next"]
                            xmaximum 0.5
                        null width 20
                        label "{color=#FFF}%d{/color}" % mySoul["next"]
                    null height 30
                    hbox:
                        label "{color=#FFF}%s %d{/color}" % (_("Level"), mySoul["level"])
                        #button Level up
                    null height 30
                    ## TODO: Other stuff
                    null height 30
                    label "{color=#FFF}%s{/color}" % _("Scene Recollection")
                    vpgrid:
                        cols 2
                        spacing 5
                        draggable True
                        mousewheel True
                        scrollbars "vertical"
                        side_xalign 0.5
                        if mySoul["level"] >= 0:
                            textbutton _("Prologue"):
                                background Frame("gui/frame.png", 0, 0)
                                xysize (200, 40)
                                action Return("intro")
                                tooltip _("Replay the prologue.")
                        if persistent.firstlake:
                            textbutton _("First Lake"):
                                background Frame("gui/frame.png", 0, 0)
                                xysize (200, 40)
                                action Return("ch1lake")
                                tooltip _("Replay the first lake.")
                        if mySoul["level"] >= 1:
                            textbutton _("Chapt. 2"):
                                background Frame("gui/frame.png", 0, 0)
                                xysize (200, 40)
                                action Return("ch2intro")
                                tooltip _("Replay the 2nd Intro.")
                        if persistent.secondlake:
                            textbutton _("Second Lake"):
                                background Frame("gui/frame.png", 0, 0)
                                xysize (200, 40)
                                action Return("ch2lake")
                                tooltip _("Replay the 2nd lake.")
        frame:
            yalign 0.9
            xalign 0.9
            xmaximum 240
            button:
                xmaximum 240
                ymaximum 40
                action Return(None)
                fixed:
                    #add Frame("gui/button/choice_hover_background.png", 0, 0)
                    text _("Return »"):
                        color "#000"
                        xalign 0.5
                        yalign 0.5



#################################################################################
init python:
    class ManaSparkle(object):
        def __init__(self, spd=25, multi=True):
            self.sm = SpriteManager(update=self.update)
            self.items = [ ]
            if multi:
                d = Transform("images/EnBallBlue.png", zoom=.05)
                for i in range(0, 80):
                    self.add(d, renpy.random.randint(spd, spd*2))
            d = Transform("images/EnBallBlue.png", zoom=.10)
            for i in range(0, 45):
                self.add(d, renpy.random.randint(spd, spd*2))
            d = Transform("images/EnBallBlue.png", zoom=.25)
            for i in range(0, 10):
                self.add(d, renpy.random.randint(spd, spd*2))
            d = Transform("images/EnBallBlue.png", zoom=.40)
            for i in range(0, 5):
                self.add(d, renpy.random.randint(spd, spd*2))
        def add(self, d, speed):
            s = self.sm.create(d)
            startx = renpy.random.randint(0, 1200)
            starty = renpy.random.randint(400, 720)
            vect = renpy.random.randint(0, 4)
            self.items.append((s, startx, starty, vect, speed))
        def update(self, st):
            for s, startx, starty, vect, speed in self.items:
                if vect % 2 == 0:
                    s.x = (startx + speed * st) % 1300
                else:
                    s.x = (startx - speed * st) % 1300
                if vect % 2 == 1:
                    s.y = (starty - speed * st) % 800
                else:
                    s.y = (starty + speed * st) % 800
            return 0

#################################################################################
label thevoid:
    $ progress = 0
    $ mySoul = None
    $ renpy.invoke_in_thread(load_souldata)
    scene DKBG
    show expression (ManaSparkle().sm) as flare
    with Dissolve(1.0)
    # Block the main thread until the socket connection is done
    show expression Text(_("{color=#FFF}Loading user data...{/color}")) at truecenter as loader
    with None
    python:
        while progress < 100:
            if responsive:
                sdelay()
            else:
                responsive = True
                break
    hide loader with None
    if mySoul is not None:
        ## Setup
        python:
            if persistent.steam and steam.initialized:
                mySoul["name"] = steam.get_persona_name()
            else:
                mySoul["name"] = _("Wanderer")

        ## You may have leveled up - catch it first and show cutscene
        if mySoul["up"]:
            if (mySoul["level"] == 0):
                call intro
            elif (mySoul["level"] == 1):
                call ch2intro

        ## Loop
        $ loop = True
        while loop:
            call screen souldata()
            if isinstance(_return, str):
                $ renpy.call_in_new_context(_return)
            else:
                $ loop = False
        $ del loop
    scene black with Dissolve(1.5)
    pause 0.15
    return

#################################################################################
label intro:
    $ RPCUpdate("The Void", "launcher")
    scene DKBG
    show expression (ManaSparkle().sm) as flare
    show DKSD at left
    with Dissolve(1.0)
    evil "{cps=80}Look at this... All this mana... Doesn't it look peaceful?{/cps}"
    evil "{cps=80}What do you think that would happen if I mess with it... like this?!{/cps}{nw}"
    hide flare
    show expression (ManaSparkle(spd=100).sm) as flare behind DKSD
    with None
    evil "What do you think that would happen if I mess with it... like this?!{fast}{nw}" with vpunch
    evil "What do you think that would happen if I mess with it... like this?!{fast}{nw}" with hpunch
    evil "What do you think that would happen if I mess with it... like this?!{fast}{nw}" with vpunch
    evil "What do you think that would happen if I mess with it... like this?!{fast}" with hpunch
    evil "{cps=80}...What? No, you cannot stop me.{/cps}"
    evil "{i}*evil grin*{/i} {cps=80}Hey, let's play a game.{/cps}"
    evil "{cps=80}I'll shatter your Soul and I want see if you can catch me!{/cps}"
    evil "{cps=80}HAHAHAHAHAHA--{/cps}{w=.5}{nw}"
    hide flare with dissolve
    show black behind DKSD with dissolve
    scene black with None # fade out to black
    evil "HAHAHAHAHAHA{fast}{cps=80}HAHAHAHAHAHAHAH!{/cps}"
    window hide
    show MLP at truecenter with Dissolve(1.0) # TODO: Locale
    pause 2.5
    scene black with Dissolve(1.0)
    show expression (ManaSparkle(spd=10, multi=False).sm) as flare
    with Dissolve(5.0)
    centered "{color=#fff}{cps=80}Having your soul shattered to pieces, you drift along the void...{p=1.5}\n\
Your soul was shattered, your memories forfeit, but your willpower remained.{p=1.5}\n\
That's right - You have a task to do, a mission to accomplish.{p=1.5}\n\
\"This is not the end.\" You think to yourself.{p=1.5}\n\
So, you dive in the Mirror Lake, allowing your willpower to become part of the worlds which came to be...{/cps}{/color}"
    window hide
    pause 0.5
    hide flare with dissolve
    centered "{color=#fff}{b}Tutorial{/b}\n\n\
In the next screen, you'll be at the Mirror Lake Interface.\nThe worlds are listed to the left, and a description is available at right.\n\
Select any world, and allow your willpower to take form on the world, as one of its inhabitants.\n\
Each world is different from each other, there is no consistency on gameplay rules, art style or history.\n\
\n\
And eventually, your power shall return to you...{/color}{fast}"
    centered "{color=#f35}{b}DISCLAIMER{/b}{/color}\n\n\
{color=#fff}The worlds may be operated by different staff, and are in an eternal mutation state.\n\
Be sure to read each subworld specific rules and complete the tutorial to spot the differences!\n\
\n\
Attempt to restore your soul as you try to stop the evil which threatens the multiverse.\n\
Soul Level remains constant regardless of the subworld you're at.{/color}{fast}"
    scene black
    return

#################################################################################
label ch1lake:
    $ RPCUpdate("The Void", "launcher")
    scene black
    show aethyr1
    #show aethyr2
    show expression (ManaSparkle().sm) as flare
    with Dissolve(1.0)
    centered "{color=#fff}{cps=80}As you move through the worlds, you can't help but notice the precarious state it is in.{p=1.5}\n\
Maybe, from within, everything seemed alright. Or just manageable. But...{p=1.5}\n\
No, the situation is much more dire than you thought.{p=1.5}\n\
Because from the Mirror Lake, you can see.{p=1.5}\n\
The foundation of the world is rotting, without anyone noticing.{/cps}{/color}"
    window hide
    pause 0.5
    hide aethyr1
    with dissolve
    hide flare
    with dissolve
    scene DKBG with dissolve
    centered "{color=#fff}{cps=80}A slow death, where the colors from the world slowly vanish.{p=1.5}\n\
The wind grows silent, the restlessness of the living slowly drifts in the peaceful calm of the dead...{p=1.5}\n\
...Is this how everything will end?{p=1.5}\n\
...Isn't there anything you can do?{/cps}{/color}"
    window hide
    pause 0.5
    show expression (ManaSparkle(spd=120).sm) as flare at bgobj
    with Dissolve(3.0)
    centered "{color=#fff}{cps=80}\"It's useless. He'll do as he pleases, even if half of the multiverse dies from it.\"{p=1.5}\n\
No. That will not happen.{p=1.5}\n\
And it won't happen, because {i}you{/i} noticed it.{p=1.5}\n\
As long as you know, as long as you breathe, something can be done.{p=1.5}\n\
Even without knowing from where to start...{/cps}{/color}"
    hide flare with dissolve
    centered "{color=#fff}{cps=80}You may be the only one left who can at least try.{/cps}{/color}"
    return


#    centered "{color=#fff}{cps=80}\
#{p=1.5}\n\
#{/cps}{/color}"
#################################################################################
label ch2intro:
    $ RPCUpdate("The Void", "launcher")
    scene black with None # fade out to black
    scene DKBG
    window hide
    show expression (ManaSparkle(spd=10, multi=False).sm) as flare
    with Dissolve(1.0)
    centered "{color=#fff}{cps=80}\
...Where am I?{p=1.5}\n\
...Oh, that's right.{p=1.5}\n\
We're at the void. The space between the universes.\
{/cps}{/color}"
    centered "{color=#fff}{cps=80}\
...How many universes are out there?{p=1.5}\n\
Now, isn't that a silly question? There are as many as there could be.{p=1.5}\n\
That's the not the real question. The real question would be...{p=1.5}\n\
...From which one do I come from...?{p=1.5}\n\
{/cps}{/color}"
    window hide
    pause 0.5
    centered "{color=#fff}{cps=80}\
The multiverse seems inquiet.{p=1.5}\n\
Has it always been this way?{p=1.5}\n\
Who is this shadow I keep remembering during my dreams?{p=1.5}\n\
{/cps}{/color}"
    centered "{color=#fff}{cps=80}\
Many questions, but few answers.{p=1.5}\n\
With what should we worry most, the past or the present?{p=1.5}\n\
However... These universes must hold a clue.{p=1.5}\n\
Coincidences are mere opportunities waiting to be explored; To reveal a new perspective to you.{p=1.5}\n\
We should investigate these Mirror Lakes, the location where we move between worlds.{p=1.5}\n\
If I want a clue... Yes, that'll be the first place to search.{p=1.5}\n\
{/cps}{/color}"
    pause 0.5
    hide flare
    show expression Text(_("{color=#fff}{cps=80}Your memories still aren't back; But you're not going to be defeated so easily.{w=0.5}\n\nYou'll get to the bottom of this. Nothing can stop you now.{/cps}{/color}")) at truecenter as laby
    with Dissolve(3.0)
    pause 2.0
    hide laby with dissolve
    centered "{color=#fff}CHAPTER 2\nSHADOWS OF THE PAST{/color}"
    scene black
    return

#################################################################################
label ch2lake:
    $ RPCUpdate("The Void", "launcher")
    scene black
    show aethyr1
    #show aethyr2
    show expression (ManaSparkle(spd=20, multi=False).sm) as flare
    with Dissolve(1.0)
    centered "{color=#fff}{cps=80}We made many acquaintances and friends lately.{p=1.5}\n\
\"Your friends shall be your strength\".{p=1.5}\n\
 Where have I heard that before?{p=1.5}\n\
And on the meanwhile, the world slowly rots, willing to take all my acquaintances into a silent death...{/cps}{/color}"
    window hide
    pause 0.5
    hide flare
    with dissolve
    centered "{color=#fff}{cps=80}But something changed.{p=1.5}\n\
This lake is less rotten than the first time.{p=1.5}\n\
...Is this my own doing?{p=1.5}\n\
...Am I truly capable to breath new life in this world?{/cps}{/color}"
    window hide
    pause 0.5
    show expression (ManaSparkle(spd=90).sm) as flare at bgobj
    with Dissolve(3.0)
    centered "{color=#fff}{cps=80}\"Either try or give up, there's no middle term between both\".{p=1.5}\n\
I can feel despair, the imminent chaos approaching...{p=1.5}\n\
But I don't have to be alone.{p=1.5}\n\
Protecting the worlds... My native acquaintances likely could help.{p=1.5}\n\
I should find those whom can help, and prepare an army.{p=1.5}\n\
Because... Whatever is coming, is coming soon.{/cps}{/color}"
    hide aethyr1 with Dissolve(3.0)
    centered "{color=#fff}{cps=80}A ray of hope.\n\nThis is not what you asked to be, but you will fulfill the role given to you.\n\n\nAnd thus, you dive again, in the depths of the Mirror Lake...{/cps}{/color}"
    return

