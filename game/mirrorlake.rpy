#################################################################################
#     This file is part of Mana Launcher.
#     Copyright (C) 2021  Jesusalva <jesusalva@tmw2.org>
#
#     Distributed under the MIT license, except for Steam parts.
#################################################################################
screen mirrorlake():
    default server = None
    add "images/default.png"

    fixed:
      viewport:
        mousewheel True
        scrollbars "vertical"
        arrowkeys True
        pagekeys True
        yalign 0.5
        xalign 0.2
        child_size (350, 650)
        xmaximum 350
        xminimum 350
        ymaximum 650
        yminimum 650
        vbox:
            ## The Void
            button:
              action SetScreenVariable("server", None)
              fixed:
                xmaximum 320
                ymaximum 40
                xalign 0.5
                add Frame("gui/button/choice_hover_background.png", 0, 0)
                text _("{b}The Void{/b}"):
                    color "#F2F"
                    xalign 0.5
                    yalign 0.5

            ## Normal Worlds
            for (idx, srv) in enumerate(persistent.serverlist):
                button:
                    action SetScreenVariable("server", idx)
                    fixed:
                        xmaximum 320
                        ymaximum 40
                        xalign 0.5
                        add Frame("gui/button/choice_hover_background.png", 0, 0)
                        text _("%s" % srv["Name"]):
                            color "#FFF"
                            xalign 0.5
                            yalign 0.5
            button:
                action Return("QUIT")
                fixed:
                    xmaximum 320
                    ymaximum 40
                    xalign 0.5
                    add Frame("gui/button/choice_hover_background.png", 0, 0)
                    text _("Quit"):
                        color "#FFF"
                        xalign 0.5
                        yalign 0.5

      fixed:
        xalign 0.9
        yalign 0.5
        xminimum 600
        xmaximum 600
        yminimum 650
        ymaximum 650
        #add "back" TODO
        if server is not None:
          vbox:
            text _("%s" % persistent.serverlist[server]["Name"]):
                size 36
                font "f/Jura-Regular.otf"
                color "#FFF"
            null height 20
            hbox:
                text _("{a=%s}Website{/a}" % persistent.serverlist[server]["Link"])
                null width 20
                text "|"
                null width 20
                text _("{a=%s}Support{/a}" % persistent.serverlist[server]["Help"])
                null width 20
                text _("{a=%s}News{/a}" % persistent.serverlist[server]["News"])
                null width 20
                text _("{a=%s}Terms of Use{/a}" % persistent.serverlist[server]["Policy"])
            null height 40
            text _("%s" % persistent.serverlist[server]["Desc"]):
                size 23
                color "#FFF"
            null height 40
            # Handle Online List
            text _("Online: %s players" % (onl_cnt(persistent.serverlist[server]))):
                size 18
                color "#F77"
            text _(onl_list(persistent.serverlist[server])):
                size 14
                color "#F77"
          hbox:
            yalign 0.9
            xalign 0.9
            xmaximum 320
            button:
                xmaximum 320
                ymaximum 40
                action Return(server)
                fixed:
                    add Frame("gui/button/choice_hover_background.png", 0, 0)
                    text _("Play!"):
                        color "#FFF"
                        xalign 0.5
                        yalign 0.5
        if server is None:
          vbox:
            text _("The Void"):
                size 36
                font "f/Jura-Regular.otf"
                color "#FFF"
            null height 80
            text _("Restore your soul from the multiple worlds where it was scattered, and in due time, The Void shall reveal to you, the truth of this multiverse..."):
                size 24
                color "#FFF"
            null height 40
          hbox:
            yalign 0.9
            xalign 0.9
            xmaximum 320
            button:
                xmaximum 320
                ymaximum 40
                action Return(-1)
                fixed:
                    add Frame("gui/button/choice_hover_background.png", 0, 0)
                    text _("Play!"):
                        color "#FFF"
                        xalign 0.5
                        yalign 0.5

#################################################################################
label intro_moubootaurlegends:
    show expression Text(_("In a boundless dimension, time didn't exist.\n\
And in an azure color, lies Mana.\n\
Peace. Quietude. The Mana is there, as it always have been.\n\
\n\
Until one day. Nobody knows who or what stirred this, but maybe you do.\n\
Nonetheless, Mana started to move restlessy, as time and space formed around it.\n\
\n\
The peaceful azure was no more, infinite combinations all at once,\n\
everything came to be and at same time, none of that ever happened.\n\
\n\
Reality starts defining itself, new spaces, new times,\n\
new realities happening and vanishing forever.\n\
Eventually, this reality broke into other minor ones, and story was rewritten.\n\
\n\
And in one of the minor realities which came to be from the bigger one...\n\
Lies this small world, the last to appear...\n\
\n\
{b}Moubootaur Legends{/b}."), color="#fff") as intr:
        xalign 0.5
        yanchor 0.0
        ypos 1.05
        linear 30 ypos -0.7
    pause 30
    hide intr with dissolve
    return
#################################################################################
label intro_themanaworld_evolved:
    show expression Text(_("Before the universe existed as it is known,\n\
There was only mana, and a sphere of energy to which mana was attracted.\n\
Mana surrounded the sphere in all directions.\n\
\n\
Then, an anomaly. For a reason which perhaps you may know, the sphere expanded.\n\
With great force in all directions, chaos ensued.\n\
Sentient beings would be formed and dissolved soon after.\n\
Eventually, one of these managed to grasp their own existence.\n\
Soon after, the Great Dragons formed a society, and peace ensued.\n\
\n\
The Mana source formed, a tree expanding from it and stretching high.\n\
Wars happened for the source, and soon, the Great Dragons were only four.\n\
But the peace returned, and soon they forgot from each other.\n\
\n\
But then came greed, with greed a war, and one of them perished.\n\
\n\
This unleashed chaos again in the world, and in a call for adventurers,\n\
many decided to risk their lives in a journey to Candor Island in...\n\
\n\
{b}The Mana World: Evolved{/b}."), color="#fff") as intr:
        xalign 0.5
        yanchor 0.0
        ypos 1.05
        linear 35 ypos -0.75
    pause 35
    hide intr with dissolve
    return
#################################################################################
label intro_themanaworld_revolt:
    show expression Text(_("Nobody knows how this world came to be.\n\
The many wars have long destroyed any record worth consideration.\n\
But everyone knows about one tree.\n\
The Mana Tree.\n\
And from it, life flows in the world.\n\
\n\
But one day, there was a war, and the tree was destroyed.\n\
But life did not stop. So the tree must still exist.\n\
Somewhere. Sometime. Beyond mortals' understanding.\n\
\n\
None realize, but this world came from another.\n\
And in the another world, there was an enemy who destroyed many towns.\n\
Adventurers were on pursuit, when something happened.\n\
\n\
Maybe you know, maybe you don't...\n\
But these adventurers shall once again regroup, and finish what was started...\n\
\n\
{b}The Mana World: rEvolt{/b}."), color="#fff") as intr:
        xalign 0.5
        yanchor 0.0
        ypos 1.05
        linear 35 ypos -0.75
    pause 35
    hide intr with dissolve
    return

