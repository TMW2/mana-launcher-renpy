#################################################################################
#     This file is part of Mana Launcher.
#     Copyright (C) 2021  Jesusalva <jesusalva@tmw2.org>
#
#     Distributed under the MIT license.
#################################################################################
screen loading():
    zorder 100

    fixed:
        frame:
            xfill True
            yfill True
            background Frame("images/default.png", 0, 0)
        bar:
            value progress
            range 100
            xalign 0.5
            yalign 0.45
            xmaximum 0.75
        label "[statusmsg]":
            xalign 0.5
            yalign 0.55

screen serverdata(server):
    zorder 100

    fixed:
        frame:
            xfill True
            yfill True
            background Frame("images/default.png", 0, 0)
        label "{b}%s{/b}" % persistent.serverlist[server]["Name"]:
            xalign 0.5
            yalign 0.2
        label "{color=#fff}[statusmsg]{/color}":
            xalign 0.5
            yalign 0.5
        hbox:
            xalign 0.5
            yalign 0.8
            text _("{a=%s}Website{/a}" % persistent.serverlist[server]["Link"])
            null width 20
            text "|"
            null width 20
            text _("{a=%s}Support{/a}" % persistent.serverlist[server]["Help"])
            null width 20
            text _("{a=%s}News{/a}" % persistent.serverlist[server]["News"])
            null width 20
            text _("{a=%s}Terms of Use{/a}" % persistent.serverlist[server]["Policy"])


# The game starts here.
label start:
    ## Check for steam moduli
    python:
        if persistent.steam:
            stdout("Steam Module verification...")
            try:
                steam.init()
            except:
                import _renpysteam as steam

    ## Begin formally
    $ statusmsg=_("Validating SSL Certificates...")
    scene black

    show screen loading
    call before_main_menu
    $ stdout("Before menu OK")

    # Build the Vault PEM, blocking and fatal
    $ build_vault()
    $ stdout("Vault PEM OK")

    # Run updater
    $ renpy.invoke_in_thread(CONFIGURE_LAUNCHER)
    $ stdout("Thread start OK")

    # Estabilish the connection to server
    python:
        # Block the main thread until the socket connection is done
        while progress < 100:
            if responsive:
                sdelay()
            else:
                break

            # Maybe we are waiting some prompt
            if SCR_PROMPT is not None:
                SCR_RESULT = renpy.call_screen("confirm", SCR_PROMPT,
            Return(True), Return(False))
                SCR_PROMPT = None


    $ stdout("Thread finish OK")

    # Kill the program
    if not responsive:
        jump die

    # We're running, keep online lists up-to-date in background
    $ running = True
    $ renpy.invoke_in_thread(ONLINE_LISTING)

    # Do we have a Vault ID?
    if not vaultId:
        call register

    # If we still don't have a Vault ID - something went wrong D:
    if not vaultId:
        $ status_update(_("{color=#F00}Failure! Vault ID could not be set.{/color}"))
        jump die

    $ stdout("Connection established! User ID %d" % vaultId)

    # Open game
    hide screen loading
    scene black
    if persistent.vaultId is None or persistent.vaultId != vaultId:
        $ persistent.vaultId = vaultId
        call intro
        scene black with None
    show TMW2 at truecenter
    with Dissolve(0.5)
    pause 1.0
    hide TMW2 with dissolve
    #centered "Error"
    #if config.developer:
    #    "User ID: [vaultId]"

    ## Automatic world selection from commandline
    if (AUTOWORLD):
        $ _return = AUTOWORLD
        jump load_world

label start_loop:
    $ renpy.block_rollback()
    call screen mirrorlake
    ## Quit request
    if _return is None or _return == "QUIT":
        $ stdout("Good bye!")
        $ renpy.quit()
        return
    ## Asked for The Void
    if _return < 0:
        $ stdout("V0id")
        call thevoid
        jump start_loop
label load_world:
    ## Asked for any subworld
    scene black
    # FIXME: Improve this waiting screen
    # Possibly retrieve messages from the thread
    $ statusmsg=_("Now loading...")
    show screen serverdata(_return)
    with None
    pause 0.01
    python:
        renpy.invoke_in_thread(launch_game, _return)
        progress = 0
        # Block the main thread until the socket connection is done
        while progress < 100:
            # Do we get a cutscene?
            if SCR_CUTIN:
                try:
                    renpy.call_in_new_context(SCR_CUTIN)
                except:
                    traceback.print_exc()
                    pass
                SCR_CUTIN=""
            if responsive:
                sdelay()
            else:
                break

            # Maybe we are waiting some prompt
            if SCR_PROMPT is not None:
                SCR_RESULT = renpy.call_screen("confirm", SCR_PROMPT,
            Return(True), Return(False))
                SCR_PROMPT = None

    hide screen serverdata
    # Kill the program instead of looping back
    if not responsive:
        scene black
        centered "{color=#FFF}{b}An error happened.{/b}\n\nPlease login again. If this issue persists, relaunch the app or {a=https://discord.gg/BQNTe68}contact us{/a}{/color}"
        $ stdout("Program died - unsucessful game launch")
        return

    ## Requested a Mirror Lake
    if MLP_DEST is not None:
        $ stdout("MLP Destination is set")
        python:
            try:
                _return=dl_search_idx(persistent.serverlist, "UUID", MLP_DEST)
                _return=int(_return)
            except:
                traceback.print_exc()
                _return=-1
            MLP_DEST=None

        ## We may need to do a cutscene, first
        if not persistent.firstlake:
            call ch1lake
            $ persistent.firstlake = True
        elif mySoul is not None:
            ## Level-based cutscenes, visit the Void first to see (missable)
            if mySoul["level"] >= 1 and not persistent.secondlake:
                call ch2lake
                $ persistent.secondlake = True

        ## Internal Sanitization
        $ stdout("WID Target: %d" % (_return))
        if _return > 0:
            jump load_world

    # Automatically loop back
    jump start_loop

