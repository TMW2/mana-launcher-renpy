﻿# TODO: Translation updated at 2021-05-18 13:52

translate pt_br strings:

    # game/update.rpy:278
    old "Email Auth"
    new "Por Email"

    # game/update.rpy:279
    old "Pass+2FA Auth"
    new "Senha+2FA"

    # game/update.rpy:320
    old "OK"
    new "OK"

    # game/update.rpy:332
    old "Please make sure you enter a valid email!"
    new "Certifique-se que o email é válido!"

    # game/update.rpy:352
    old "An email was sent with your API token.\n\nYou'll need it shortly, so check your email."
    new "An email was sent with your API token.\n\nYou'll need it shortly, so check your email."

    # game/update.rpy:362
    old "{b}INTERNAL SERVER ERROR{/b}\n\nSeems like someone messed up on the APIs!\nThis login method seems to be temporaly unavailable, please choose another one."
    new "{b}ERRO INTERNO DO SERVIDOR{/b}\n\nParece que alguém bagunçou nas APIs!\nEsse método de login parece estar temporariamente indisponível, por favor escolha outro."

# TODO: Translation updated at 2021-05-19 03:14

translate pt_br strings:

    # game/update.rpy:81
    old "Fetching server list and assets..."
    new "Baixando lista de servidores..."

    # game/update.rpy:141
    old "Welcome to the Mana Launcher.\nBuilding user data, please wait..."
    new "Bem vindo ao Lançador de Mana.\nConstruindo dados do usuário, por favor aguarde..."

    # game/update.rpy:157
    old "{color=#f00}{b}ERROR: No active mirror found.\nMana Launcher needs to be updated.{/b}{/color}"
    new "{color=#f00}{b}ERRO: Nenhum espelho ativo encontrado.\nO Lançador de Mana precisa ser atualizado.{/b}{/color}"

    # game/update.rpy:166
    old "Checking for installed clients..."
    new "Verificando clientes instalados..."

    # game/update.rpy:186
    old "Verifying credentials..."
    new "Verificando credenciais..."

    # game/update.rpy:193
    old "Attempting Steam authentication..."
    new "Tentando autenticação pela Steam..."

    # game/update.rpy:201
    old "Waiting for Vault reply..."
    new "Aguardando resposta do Cofre..."

    # game/update.rpy:208
    old "{color=#f00}{b}STEAM AUTHENTICATION ERROR.\nSteam refused authentication. Try restarting the app.{/b}{/color}"
    new "{color=#f00}{b}ERRO NA AUTENTICAÇÃO STEAM.\nSteam recusou a autenticação. Tente reiniciar o aplicativo.{/b}{/color}"

    # game/update.rpy:214
    old "Rate limited! Trying again 15s..."
    new "Erro de fluxo! Tentando novamente (15s)..."

    # game/update.rpy:216
    old "Rate limited! Trying again 10s..."
    new "Erro de fluxo! Tentando novamente (10s)..."

    # game/update.rpy:218
    old "Rate limited! Trying again 5s..."
    new "Erro de fluxo! Tentando novamente (5s)..."

    # game/update.rpy:220
    old "Rate limited! Trying again..."
    new "Erro de fluxo! Tentando novamente..."

    # game/update.rpy:238
    old "Steam session initialized successfully"
    new "Sessão Steam inicializada com sucesso"

    # game/update.rpy:245
    old "{color=#f00}{b}STEAM AUTHENTICATION ERROR.\nIf the issue persists, try closing the app and opening again.{/b}{/color}"
    new "{color=#f00}{b}ERRO DE AUTENTICAÇÃO (STEAM).\nSe o problema persistir, tente fechar o aplicativo e abrir novamente.{/b}{/color}"

    # game/update.rpy:250
    old "Steam auth disabled, logging on Vault..."
    new "Steam auth desativado, logando pelo Cofre..."

    # game/update.rpy:342
    old "Please insert your {b}email{/b}."
    new "Por favor digite seu {b}email{/b}."

    # game/update.rpy:368
    old "Please insert the {b}token{/b} you received on your email."
    new "Please insert the {b}token{/b} you received on your email."

    # game/update.rpy:386
    old "Please insert your {b}Password{/b}.\nIt has to be at least 4 characters long."
    new "Por favor digite sua {b}Senha{/b}.\nEla tem que ter pelo menos 4 caracteres."

    # game/update.rpy:392
    old "Please insert your {b}2FA code{/b}. If you do not have 2FA, leave blank.\n\n{u}TOTP setup will be emailed and required for later logins.{/u}"
    new "Por favor digite seu {b}código de 2 fatores{/b}. Se você não possui 2 fatores, deixe em branco.\n\n{u}Você receberá a chave TOTP por email e será necessária para logar posteriormente.{/u}"

    # game/update.rpy:437
    old "Success!"
    new "Sucesso!"

    # game/update.rpy:439
    old "{color=#F00}Failure!{/color}"
    new "{color=#F00}Fracasso!{/color}"

    # game/update.rpy:444
    old "Please insert your {b}2FA Secret{/b} or the link sent to you by email.\n\n{size=18}{color=#f00}WARNING:{/color} Will be saved locally with minimal security.{/size}"
    new "Por favor insira seu {b}Segredo 2FA{/b} ou o link enviado a você por email.\n\n{size=18}{color=#f00}AVISO:{/color} Será salvo localmente com segurança mínima.{/size}"

    # game/update.rpy:453
    old "Invalid OTPAuth URL.\nEnsure you used the URL sent to you by email!"
    new "URL OTPAuth Inválida.\nCertifique-se que você utilizou a URL enviada por email!"

