﻿# TODO: Translation updated at 2021-05-18 13:52

translate pt_br strings:

    # game/screens.rpy:256
    old "History"
    new "Histórico"

    # game/screens.rpy:257
    old "Skip"
    new "Pular"

    # game/screens.rpy:258
    old "Prefs"
    new "Prefs"

    # game/screens.rpy:299
    old "Start"
    new "Iniciar"

    # game/screens.rpy:306
    old "Preferences"
    new "Preferências"

    # game/screens.rpy:310
    old "End Replay"
    new "Encerrar Replay"

    # game/screens.rpy:314
    old "Main Menu"
    new "Menu Principal"

    # game/screens.rpy:316
    old "About"
    new "Sobre"

    # game/screens.rpy:322
    old "Quit"
    new "Sair"

    # game/screens.rpy:467
    old "Return"
    new "Voltar"

    # game/screens.rpy:551
    old "Version [config.version!t]\n"
    new "Versão [config.version!t]\n"

    # game/screens.rpy:557
    old "Made with {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"
    new "Feito com {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"

    # game/screens.rpy:647
    old "Display"
    new "Exibição"

    # game/screens.rpy:648
    old "Window"
    new "Janela"

    # game/screens.rpy:649
    old "Fullscreen"
    new "Tela Cheia"

    # game/screens.rpy:650
    old "Iconify"
    new "Minimizar"

    # game/screens.rpy:651
    old "Enabled"
    new "Ativado"

    # game/screens.rpy:651
    old "Disabled"
    new "Desativado"

    # game/screens.rpy:658
    old "Discord"
    new "Discord"

    # game/screens.rpy:664
    old "Steam Login"
    new "Steam Login"

    # game/screens.rpy:670
    old "Game Client"
    new "Cliente de Jogo"

    # game/screens.rpy:671
    old "ManaPlus"
    new "ManaPlus"

    # game/screens.rpy:673
    old "Mana"
    new "Mana"

    # game/screens.rpy:679
    old "Language"
    new "Idioma"

    # game/screens.rpy:683
    old "Unseen Text"
    new "Texto Não Lido"

    # game/screens.rpy:684
    old "After Choices"
    new "Após Escolhas"

    # game/screens.rpy:685
    old "Transitions"
    new "Transições"

    # game/screens.rpy:705
    old "Music Volume"
    new "Music Volume"

    # game/screens.rpy:712
    old "Sound Volume"
    new "Sound Volume"

    # game/screens.rpy:718
    old "Test"
    new "Teste"

    # game/screens.rpy:722
    old "Voice Volume"
    new "Voice Volume"

    # game/screens.rpy:733
    old "Mute All"
    new "Mute All"

    # game/screens.rpy:739
    old "Check Files Integrity"
    new "Conferir Integridade dos Arquivos"

    # game/screens.rpy:756
    old "All Set!"
    new "Tudo Pronto!"

    # game/screens.rpy:876
    old "The dialogue history is empty."
    new "Não há mensagens para mostrar."

    # game/screens.rpy:937
    old "Help"
    new "Ajuda"

    # game/screens.rpy:946
    old "Keyboard"
    new "Teclado"

    # game/screens.rpy:947
    old "Mouse"
    new "Mouse"

    # game/screens.rpy:950
    old "Gamepad"
    new "Gamepad"

    # game/screens.rpy:963
    old "Enter"
    new "Entrar"

    # game/screens.rpy:964
    old "Advances dialogue and activates the interface."
    new "Advances dialogue and activates the interface."

    # game/screens.rpy:967
    old "Space"
    new "Space"

    # game/screens.rpy:968
    old "Advances dialogue without selecting choices."
    new "Advances dialogue without selecting choices."

    # game/screens.rpy:971
    old "Arrow Keys"
    new "Arrow Keys"

    # game/screens.rpy:972
    old "Navigate the interface."
    new "Navigate the interface."

    # game/screens.rpy:975
    old "Escape"
    new "Escape"

    # game/screens.rpy:976
    old "Accesses the game menu."
    new "Accesses the game menu."

    # game/screens.rpy:979
    old "Ctrl"
    new "Ctrl"

    # game/screens.rpy:980
    old "Skips dialogue while held down."
    new "Skips dialogue while held down."

    # game/screens.rpy:983
    old "Tab"
    new "Tab"

    # game/screens.rpy:984
    old "Toggles dialogue skipping."
    new "Toggles dialogue skipping."

    # game/screens.rpy:988
    old "Hides the user interface."
    new "Hides the user interface."

    # game/screens.rpy:992
    old "Takes a screenshot."
    new "Takes a screenshot."

    # game/screens.rpy:996
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."

    # game/screens.rpy:1002
    old "Left Click"
    new "Left Click"

    # game/screens.rpy:1006
    old "Middle Click"
    new "Middle Click"

    # game/screens.rpy:1010
    old "Right Click"
    new "Right Click"

    # game/screens.rpy:1017
    old "Right Trigger\nA/Bottom Button"
    new "Right Trigger\nA/Bottom Button"

    # game/screens.rpy:1021
    old "D-Pad, Sticks"
    new "D-Pad, Sticks"

    # game/screens.rpy:1025
    old "Start, Guide"
    new "Start, Guide"

    # game/screens.rpy:1029
    old "Y/Top Button"
    new "Y/Top Button"

    # game/screens.rpy:1032
    old "Calibrate"
    new "Calibrar"

    # game/screens.rpy:1097
    old "Yes"
    new "Sim"

    # game/screens.rpy:1098
    old "No"
    new "Não"

    # game/screens.rpy:1144
    old "Skipping"
    new "Pulando"

    # game/screens.rpy:1365
    old "Menu"
    new "Menu"

# TODO: Translation updated at 2021-05-19 03:14

translate pt_br strings:

    # game/screens.rpy:366
    old "Mirror Lake"
    new "Lago Espelhado"

    # game/screens.rpy:699
    old "Others"
    new "Outros"

    # game/screens.rpy:700
    old "ON"
    new "ON"

    # game/screens.rpy:700
    old "OFF"
    new "OFF"

    # game/screens.rpy:706
    old "Check Updates"
    new "Buscar Atualizações"

