﻿# TODO: Translation updated at 2021-05-18 14:35

translate de strings:

    # game/update.rpy:278
    old "Email Auth"
    new "Por Email"

    # game/update.rpy:279
    old "Pass+2FA Auth"
    new "Senha+2FA"

    # game/update.rpy:320
    old "OK"
    new "OK"

    # game/update.rpy:332
    old "Please make sure you enter a valid email!"
    new "Certifique-se que o email é válido!"

    # game/update.rpy:352
    old "An email was sent with your API token.\n\nYou'll need it shortly, so check your email."
    new "An email was sent with your API token.\n\nYou'll need it shortly, so check your email."

    # game/update.rpy:362
    old "{b}INTERNAL SERVER ERROR{/b}\n\nSeems like someone messed up on the APIs!\nThis login method seems to be temporaly unavailable, please choose another one."
    new "{b}INTERNAL SERVER ERROR{/b}\n\nSeems like someone messed up on the APIs!\nThis login method seems to be temporaly unavailable, please choose another one."

# TODO: Translation updated at 2021-05-19 03:14

translate de strings:

    # game/update.rpy:81
    old "Fetching server list and assets..."
    new "Fetching server list and assets..."

    # game/update.rpy:141
    old "Welcome to the Mana Launcher.\nBuilding user data, please wait..."
    new "Welcome to the Mana Launcher.\nBuilding user data, please wait..."

    # game/update.rpy:157
    old "{color=#f00}{b}ERROR: No active mirror found.\nMana Launcher needs to be updated.{/b}{/color}"
    new "{color=#f00}{b}ERROR: No active mirror found.\nMana Launcher needs to be updated.{/b}{/color}"

    # game/update.rpy:166
    old "Checking for installed clients..."
    new "Checking for installed clients..."

    # game/update.rpy:186
    old "Verifying credentials..."
    new "Verifying credentials..."

    # game/update.rpy:193
    old "Attempting Steam authentication..."
    new "Attempting Steam authentication..."

    # game/update.rpy:201
    old "Waiting for Vault reply..."
    new "Waiting for Vault reply..."

    # game/update.rpy:208
    old "{color=#f00}{b}STEAM AUTHENTICATION ERROR.\nSteam refused authentication. Try restarting the app.{/b}{/color}"
    new "{color=#f00}{b}STEAM AUTHENTICATION ERROR.\nSteam refused authentication. Try restarting the app.{/b}{/color}"

    # game/update.rpy:214
    old "Rate limited! Trying again 15s..."
    new "Rate limited! Trying again 15s..."

    # game/update.rpy:216
    old "Rate limited! Trying again 10s..."
    new "Rate limited! Trying again 10s..."

    # game/update.rpy:218
    old "Rate limited! Trying again 5s..."
    new "Rate limited! Trying again 5s..."

    # game/update.rpy:220
    old "Rate limited! Trying again..."
    new "Rate limited! Trying again..."

    # game/update.rpy:238
    old "Steam session initialized successfully"
    new "Steam session initialized successfully"

    # game/update.rpy:245
    old "{color=#f00}{b}STEAM AUTHENTICATION ERROR.\nIf the issue persists, try closing the app and opening again.{/b}{/color}"
    new "{color=#f00}{b}STEAM AUTHENTICATION ERROR.\nIf the issue persists, try closing the app and opening again.{/b}{/color}"

    # game/update.rpy:250
    old "Steam auth disabled, logging on Vault..."
    new "Steam auth disabled, logging on Vault..."

    # game/update.rpy:342
    old "Please insert your {b}email{/b}."
    new "Please insert your {b}email{/b}."

    # game/update.rpy:368
    old "Please insert the {b}token{/b} you received on your email."
    new "Please insert the {b}token{/b} you received on your email."

    # game/update.rpy:386
    old "Please insert your {b}Password{/b}.\nIt has to be at least 4 characters long."
    new "Please insert your {b}Password{/b}.\nIt has to be at least 4 characters long."

    # game/update.rpy:392
    old "Please insert your {b}2FA code{/b}. If you do not have 2FA, leave blank.\n\n{u}TOTP setup will be emailed and required for later logins.{/u}"
    new "Please insert your {b}2FA code{/b}. If you do not have 2FA, leave blank.\n\n{u}TOTP setup will be emailed and required for later logins.{/u}"

    # game/update.rpy:437
    old "Success!"
    new "Erfolg!"

    # game/update.rpy:439
    old "{color=#F00}Failure!{/color}"
    new "{color=#F00}Failure!{/color}"

    # game/update.rpy:444
    old "Please insert your {b}2FA Secret{/b} or the link sent to you by email.\n\n{size=18}{color=#f00}WARNING:{/color} Will be saved locally with minimal security.{/size}"
    new "Please insert your {b}2FA Secret{/b} or the link sent to you by email.\n\n{size=18}{color=#f00}WARNING:{/color} Will be saved locally with minimal security.{/size}"

    # game/update.rpy:453
    old "Invalid OTPAuth URL.\nEnsure you used the URL sent to you by email!"
    new "Invalid OTPAuth URL.\nEnsure you used the URL sent to you by email!"

