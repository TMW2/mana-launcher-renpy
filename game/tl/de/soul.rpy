﻿# TODO: Translation updated at 2021-05-18 14:35

# game/soul.rpy:155
translate de intro_1d65fd1f:

    # evil "{cps=80}Look at this... All this mana... Doesn't it look peaceful?{/cps}"
    evil "{cps=80}Olhe isso... Toda essa mana... Ela não parece pacífica?{/cps}"

# game/soul.rpy:156
translate de intro_fc641e8d:

    # evil "{cps=80}What do you think that would happen if I mess with it... like this?!{/cps}{nw}"
    evil "{cps=80}O que você acha que aconteceria se eu bagunçasse com ela... desse jeito?!{/cps}{nw}"

# game/soul.rpy:160
translate de intro_774f5206:

    # evil "What do you think that would happen if I mess with it... like this?!{fast}{nw}" with vpunch
    evil "O que você acha que aconteceria se eu bagunçasse com ela... desse jeito?!{fast}{nw}" with vpunch

# game/soul.rpy:161
translate de intro_873ea3a8:

    # evil "What do you think that would happen if I mess with it... like this?!{fast}{nw}" with hpunch
    evil "O que você acha que aconteceria se eu bagunçasse com ela... desse jeito?!{fast}{nw}" with hpunch

# game/soul.rpy:162
translate de intro_774f5206_1:

    # evil "What do you think that would happen if I mess with it... like this?!{fast}{nw}" with vpunch
    evil "O que você acha que aconteceria se eu bagunçasse com ela... desse jeito?!{fast}{nw}" with vpunch

# game/soul.rpy:163
translate de intro_67488d5e:

    # evil "What do you think that would happen if I mess with it... like this?!{fast}" with hpunch
    evil "O que você acha que aconteceria se eu bagunçasse com ela... desse jeito?!{fast}" with hpunch

# game/soul.rpy:164
translate de intro_aa2c08ed:

    # evil "{cps=80}...What? No, you cannot stop me.{/cps}"
    evil "{cps=80}...O queê? Não, você não pode me deter.{/cps}"

# game/soul.rpy:165
translate de intro_96ce8641:

    # evil "{i}*evil grin*{/i} {cps=80}Hey, let's play a game.{/cps}"
    evil "{i}*evil grin*{/i} {cps=80}Hey, let's play a game.{/cps}"

# game/soul.rpy:166
translate de intro_da56d90b:

    # evil "{cps=80}I'll shatter your Soul and I want see if you can catch me!{/cps}"
    evil "{cps=80}I'll shatter your Soul and I want see if you can catch me!{/cps}"

# game/soul.rpy:167
translate de intro_6fae2a3d:

    # evil "{cps=80}HAHAHAHAHAHA--{/cps}{w=.5}{nw}"
    evil "{cps=80}HAHAHAHAHAHA--{/cps}{w=.5}{nw}"

# game/soul.rpy:171
translate de intro_88ebea11:

    # evil "HAHAHAHAHAHA{fast}{cps=80}HAHAHAHAHAHAHAH!{/cps}"
    evil "HAHAHAHAHAHA{fast}{cps=80}HAHAHAHAHAHAHAH!{/cps}"

# game/soul.rpy:178
translate de intro_19c5c775:

    # centered "{color=#fff}{cps=80}Having your soul shattered to pieces, you drift along the void...{p=1.5}\n Your soul was shattered, your memories forfeit, but your willpower remained.{p=1.5}\n That's right - You have a task to do, a mission to accomplish.{p=1.5}\n \"This is not the end.\" You think to yourself.{p=1.5}\n So, you dive in the Mirror Lake, allowing your willpower to become part of the worlds which came to be...{/cps}{/color}"
    centered "{color=#fff}{cps=80}Having your soul shattered to pieces, you drift along the void...{p=1.5}\n Your soul was shattered, your memories forfeit, but your willpower remained.{p=1.5}\n That's right - You have a task to do, a mission to accomplish.{p=1.5}\n \"This is not the end.\" You think to yourself.{p=1.5}\n So, you dive in the Mirror Lake, allowing your willpower to become part of the worlds which came to be...{/cps}{/color}"

# game/soul.rpy:186
translate de intro_82ae0cb5:

    # centered "{color=#fff}{b}Tutorial{/b}\n\n In the next screen, you'll be at the Mirror Lake Interface.\nThe worlds are listed to the left, and a description is available at right.\n Select any world, and allow your willpower to take form on the world, as one of its inhabitants.\n Each world is different from each other, there is no consistency on gameplay rules, art style or history.\n \n And eventually, your power shall return to you...{/color}{fast}"
    centered "{color=#fff}{b}Tutorial{/b}\n\n In the next screen, you'll be at the Mirror Lake Interface.\nThe worlds are listed to the left, and a description is available at right.\n Select any world, and allow your willpower to take form on the world, as one of its inhabitants.\n Each world is different from each other, there is no consistency on gameplay rules, art style or history.\n \n And eventually, your power shall return to you...{/color}{fast}"

# game/soul.rpy:192
# OBSOLETE
translate de intro_32b6ce88:

    # centered "{color=#f35}{b}DISCLAIMER{/b}\n\n {color=#fff}The worlds may be operated by different staff, and are in an eternal mutation state.\n Be sure to read each subworld specific rules and complete the tutorial to spot the differences!\n \n Attempt to restore your soul as you try to stop the evil which threatens the multiverse.\n Soul Level remains constant regardless of the subworld you're at.{/color}{fast}"
    centered "{color=#f35}{b}DISCLAIMER{/b}\n\n {color=#fff}The worlds may be operated by different staff, and are in an eternal mutation state.\n Be sure to read each subworld specific rules and complete the tutorial to spot the differences!\n \n Attempt to restore your soul as you try to stop the evil which threatens the multiverse.\n Soul Level remains constant regardless of the subworld you're at.{/color}{fast}"

translate de strings:

    # game/soul.rpy:37
    old "Level"
    new "Level"

    # game/soul.rpy:42
    old "Scene Recollection"
    new "Replay de Cenas"

    # game/soul.rpy:51
    old "Prologue"
    new "Prolog"

    # game/soul.rpy:55
    old "Replay the prologue."
    new "Exibe o prólogo novamente."

    # game/soul.rpy:66
    old "Return »"
    new "Voltar »"

    # game/soul.rpy:119
    old "{color=#FFF}Loading user data...{/color}"
    new "{color=#FFF}Carregando dados do usuário...{/color}"

    # game/soul.rpy:134
    old "Wanderer"
    new "Peregrino"

# TODO: Translation updated at 2021-05-19 03:14

# game/soul.rpy:193
translate de intro_d61d3dff:

    # centered "{color=#f35}{b}DISCLAIMER{/b}{/color}\n\n {color=#fff}The worlds may be operated by different staff, and are in an eternal mutation state.\n Be sure to read each subworld specific rules and complete the tutorial to spot the differences!\n \n Attempt to restore your soul as you try to stop the evil which threatens the multiverse.\n Soul Level remains constant regardless of the subworld you're at.{/color}{fast}"
    centered "{color=#f35}{b}DISCLAIMER{/b}{/color}\n\n {color=#fff}The worlds may be operated by different staff, and are in an eternal mutation state.\n Be sure to read each subworld specific rules and complete the tutorial to spot the differences!\n \n Attempt to restore your soul as you try to stop the evil which threatens the multiverse.\n Soul Level remains constant regardless of the subworld you're at.{/color}{fast}"

