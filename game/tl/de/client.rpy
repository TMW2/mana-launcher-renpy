﻿# TODO: Translation updated at 2021-05-18 14:35

translate de strings:

    # game/client.rpy:38
    old "Requesting credentials from Vault..."
    new "Solicitando credenciais do cofre..."

    # game/client.rpy:52
    old "You're not logged in."
    new "Você não está logado."

    # game/client.rpy:58
    old "Rate limited, we'll try again..."
    new "Erro de fluxo, vamos tentar novamente..."

    # game/client.rpy:73
    old "TMW Vault Error."
    new "Erro do Cofre TMW."

    # game/client.rpy:108
    old "Thanks for playing!"
    new "Obrigado por jogar!"

# TODO: Translation updated at 2021-06-07 15:15

translate de strings:

    # game/client.rpy:101
    old "Synchronizing data..."
    new "Synchronizing data..."

    # game/client.rpy:124
    old "Synchronization error."
    new "Synchronization error."

