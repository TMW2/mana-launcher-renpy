﻿# TODO: Translation updated at 2021-05-18 14:35

translate it strings:

    # game/update.rpy:278
    old "Email Auth"
    new "Email Auth"

    # game/update.rpy:279
    old "Pass+2FA Auth"
    new "Pass+2FA Auth"

    # game/update.rpy:320
    old "OK"
    new "OK"

    # game/update.rpy:332
    old "Please make sure you enter a valid email!"
    new "Please make sure you enter a valid email!"

    # game/update.rpy:352
    old "An email was sent with your API token.\n\nYou'll need it shortly, so check your email."
    new "An email was sent with your API token.\n\nYou'll need it shortly, so check your email."

    # game/update.rpy:362
    old "{b}INTERNAL SERVER ERROR{/b}\n\nSeems like someone messed up on the APIs!\nThis login method seems to be temporaly unavailable, please choose another one."
    new "{b}INTERNAL SERVER ERROR{/b}\n\nSeems like someone messed up on the APIs!\nThis login method seems to be temporaly unavailable, please choose another one."

