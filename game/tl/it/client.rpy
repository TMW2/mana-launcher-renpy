﻿# TODO: Translation updated at 2021-05-18 14:35

translate it strings:

    # game/client.rpy:38
    old "Requesting credentials from Vault..."
    new "Requesting credentials from Vault..."

    # game/client.rpy:52
    old "You're not logged in."
    new "You're not logged in."

    # game/client.rpy:58
    old "Rate limited, we'll try again..."
    new "Rate limited, we'll try again..."

    # game/client.rpy:73
    old "TMW Vault Error."
    new "TMW Vault Error."

    # game/client.rpy:108
    old "Thanks for playing!"
    new "Thanks for playing!"

