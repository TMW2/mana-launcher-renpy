﻿# TODO: Translation updated at 2021-05-18 14:35

translate fr strings:

    # game/screens.rpy:256
    old "History"
    new "Histórico"

    # game/screens.rpy:257
    old "Skip"
    new "Pular"

    # game/screens.rpy:258
    old "Prefs"
    new "Prefs"

    # game/screens.rpy:299
    old "Start"
    new "Iniciar"

    # game/screens.rpy:306
    old "Preferences"
    new "Preferências"

    # game/screens.rpy:310
    old "End Replay"
    new "Encerrar Replay"

    # game/screens.rpy:314
    old "Main Menu"
    new "Menu Principal"

    # game/screens.rpy:316
    old "About"
    new "Sobre"

    # game/screens.rpy:322
    old "Quit"
    new "Quitter"

    # game/screens.rpy:467
    old "Return"
    new "Voltar"

    # game/screens.rpy:551
    old "Version [config.version!t]\n"
    new "Versão [config.version!t]\n"

    # game/screens.rpy:557
    old "Made with {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"
    new "Feito com {a=https://www.renpy.org/}Ren'Py{/a} [renpy.version_only].\n\n[renpy.license!t]"

    # game/screens.rpy:647
    old "Display"
    new "Exibição"

    # game/screens.rpy:648
    old "Window"
    new "Janela"

    # game/screens.rpy:649
    old "Fullscreen"
    new "Tela Cheia"

    # game/screens.rpy:650
    old "Iconify"
    new "Minimizar"

    # game/screens.rpy:651
    old "Enabled"
    new "aktiviert"

    # game/screens.rpy:651
    old "Disabled"
    new "Deaktiviert"

    # game/screens.rpy:658
    old "Discord"
    new "Discord"

    # game/screens.rpy:664
    old "Steam Login"
    new "Steam Login"

    # game/screens.rpy:670
    old "Game Client"
    new "Cliente de Jogo"

    # game/screens.rpy:671
    old "ManaPlus"
    new "ManaPlus"

    # game/screens.rpy:673
    old "Mana"
    new "Mana"

    # game/screens.rpy:679
    old "Language"
    new "Language"

    # game/screens.rpy:684
    old "Unseen Text"
    new "Texto Não Lido"

    # game/screens.rpy:685
    old "After Choices"
    new "Após Escolhas"

    # game/screens.rpy:686
    old "Transitions"
    new "Transições"

    # game/screens.rpy:706
    old "Music Volume"
    new "Music Volume"

    # game/screens.rpy:713
    old "Sound Volume"
    new "Sound Volume"

    # game/screens.rpy:719
    old "Test"
    new "Test"

    # game/screens.rpy:723
    old "Voice Volume"
    new "Voice Volume"

    # game/screens.rpy:734
    old "Mute All"
    new "Mute All"

    # game/screens.rpy:740
    old "Check Files Integrity"
    new "Conferir Integridade dos Arquivos"

    # game/screens.rpy:757
    old "All Set!"
    new "Tudo Pronto!"

    # game/screens.rpy:877
    old "The dialogue history is empty."
    new "Não há mensagens para mostrar."

    # game/screens.rpy:938
    old "Help"
    new "Ajuda"

    # game/screens.rpy:947
    old "Keyboard"
    new "Teclado"

    # game/screens.rpy:948
    old "Mouse"
    new "Mouse"

    # game/screens.rpy:951
    old "Gamepad"
    new "Gamepad"

    # game/screens.rpy:964
    old "Enter"
    new "Entrer"

    # game/screens.rpy:965
    old "Advances dialogue and activates the interface."
    new "Advances dialogue and activates the interface."

    # game/screens.rpy:968
    old "Space"
    new "Space"

    # game/screens.rpy:969
    old "Advances dialogue without selecting choices."
    new "Advances dialogue without selecting choices."

    # game/screens.rpy:972
    old "Arrow Keys"
    new "Arrow Keys"

    # game/screens.rpy:973
    old "Navigate the interface."
    new "Navigate the interface."

    # game/screens.rpy:976
    old "Escape"
    new "Escape"

    # game/screens.rpy:977
    old "Accesses the game menu."
    new "Accesses the game menu."

    # game/screens.rpy:980
    old "Ctrl"
    new "Ctrl"

    # game/screens.rpy:981
    old "Skips dialogue while held down."
    new "Skips dialogue while held down."

    # game/screens.rpy:984
    old "Tab"
    new "Tab"

    # game/screens.rpy:985
    old "Toggles dialogue skipping."
    new "Toggles dialogue skipping."

    # game/screens.rpy:989
    old "Hides the user interface."
    new "Hides the user interface."

    # game/screens.rpy:993
    old "Takes a screenshot."
    new "Takes a screenshot."

    # game/screens.rpy:997
    old "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."
    new "Toggles assistive {a=https://www.renpy.org/l/voicing}self-voicing{/a}."

    # game/screens.rpy:1003
    old "Left Click"
    new "Left Click"

    # game/screens.rpy:1007
    old "Middle Click"
    new "Middle Click"

    # game/screens.rpy:1011
    old "Right Click"
    new "Right Click"

    # game/screens.rpy:1018
    old "Right Trigger\nA/Bottom Button"
    new "Right Trigger\nA/Bottom Button"

    # game/screens.rpy:1022
    old "D-Pad, Sticks"
    new "D-Pad, Sticks"

    # game/screens.rpy:1026
    old "Start, Guide"
    new "Start, Guide"

    # game/screens.rpy:1030
    old "Y/Top Button"
    new "Y/Top Button"

    # game/screens.rpy:1033
    old "Calibrate"
    new "Calibrar"

    # game/screens.rpy:1098
    old "Yes"
    new "Oui"

    # game/screens.rpy:1099
    old "No"
    new "Non"

    # game/screens.rpy:1145
    old "Skipping"
    new "Pulando"

    # game/screens.rpy:1366
    old "Menu"
    new "Menu"

# TODO: Translation updated at 2021-05-19 03:15

translate fr strings:

    # game/screens.rpy:366
    old "Mirror Lake"
    new "Mirror Lake"

    # game/screens.rpy:699
    old "Others"
    new "Others"

    # game/screens.rpy:700
    old "ON"
    new "ON"

    # game/screens.rpy:700
    old "OFF"
    new "OFF"

    # game/screens.rpy:706
    old "Check Updates"
    new "Check Updates"

