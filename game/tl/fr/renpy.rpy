﻿# TODO: Translation updated at 2021-05-18 14:35

# game/renpy.rpy:166
# OBSOLETE
translate fr start_loop_2dea60f4:

    # centered "{color=#FFF}{b}An error happened.{/b}\n\nPlease login again. If this issue persists, relaunch the app or {a=https://discord.gg/BQNTe68}contact us{/a}{/color}"
    centered "{color=#FFF}{b}Um erro aconteceu.{/b}\n\nPor favor, faça login novamente. Se o problema persistir, reinicie o app ou {a=https://discord.gg/BQNTe68}entre em contato conosco{/a}{/color}"

# TODO: Translation updated at 2021-05-19 03:15

translate fr strings:

    # game/renpy.rpy:62
    old "Validating SSL Certificates..."
    new "Validating SSL Certificates..."

    # game/renpy.rpy:105
    old "{color=#F00}Failure! Vault ID could not be set.{/color}"
    new "{color=#F00}Failure! Vault ID could not be set.{/color}"

    # game/renpy.rpy:142
    old "Now loading..."
    new "Now loading..."

# TODO: Translation updated at 2021-06-07 15:16

# game/renpy.rpy:176
translate fr load_world_2dea60f4:

    # centered "{color=#FFF}{b}An error happened.{/b}\n\nPlease login again. If this issue persists, relaunch the app or {a=https://discord.gg/BQNTe68}contact us{/a}{/color}"
    centered "{color=#FFF}{b}An error happened.{/b}\n\nPlease login again. If this issue persists, relaunch the app or {a=https://discord.gg/BQNTe68}contact us{/a}{/color}"

