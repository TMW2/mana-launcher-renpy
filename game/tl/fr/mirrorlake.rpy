﻿# TODO: Translation updated at 2021-05-18 14:35

translate fr strings:

    # game/mirrorlake.rpy:33
    old "{b}The Void{/b}"
    new "{b}O Vazio{/b}"

    # game/mirrorlake.rpy:104
    old "Play!"
    new "Jogar!"

    # game/mirrorlake.rpy:110
    old "The Void"
    new "O Vazio"

    # game/mirrorlake.rpy:115
    old "Restore your soul from the multiple worlds where it was scattered, and in due time, The Void shall reveal to you, the truth of this multiverse..."
    new "Restaure sua alma dos mundos ao qual ela foi espalhada, e ao seu tempo, O Vazio irá revelar a você, a verdade deste multiverso..."

