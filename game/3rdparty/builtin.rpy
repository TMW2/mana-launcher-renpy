#################################################################################
#     This file is part of Mana Launcher.
#     Copyright (C) 2022  Jesusalva <jesusalva@tmw2.org>
#
#     Distributed under the MIT license.
#     Warning: Third Party game clients
#################################################################################

init 1 python:
    import tmw
    #############################################################################
    ## Builtin client can't be auto-updated
    def download_builtin(fname):
        return True

    #######################
    ## Builtin client doesn't require installation
    def install_builtin():
        return True

    ###############################
    def cli_builtin(launch=False, download=True):
        global SCR_PROMPT, SCR_RESULT
        ##########
        if launch:
            return tmw.client
        return True

