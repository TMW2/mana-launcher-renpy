#################################################################################
#     This file is part of Mana Launcher.
#     Copyright (C) 2021  Jesusalva <jesusalva@tmw2.org>
#
#     Distributed under the MIT license.
#     Warning: Third Party game clients
#################################################################################

init 1 python:
    #############################################################################
    def download_manaplus(fname):
            installdir=get_path("manaplus")
            status_update("Downloading %s on RAM..." % fname, 62)
            r=requests.get(persistent.host+"/%s" % fname, timeout=60.0)
            if (r.status_code != 200):
                status_update("Failure retrieving M+: ERROR %d" % r.status_code)
                return False

            status_update("Saving %s..." %fname, 64)
            msize = int(r.headers.get('content-length', 0))
            bsize = 4096
            csize = msize / 7
            cstep = 0
            with open(installdir+"/%s" % fname, 'wb') as fd:
                for chunk in r.iter_content(bsize):
                    fd.write(chunk)
                    cstep += bsize
                    status_update(pc=64+(cstep / csize))

            status_update("Verifying MD5 hash...", 70)
            r=requests.get(persistent.host+"/%s.md5" % fname, timeout=10.0)
            md5up=r.text.replace("\n", "")

            status_update("Verifying MD5 hash...", 71)
            md5us=md5sum(installdir+"/%s" % fname)
            if md5up != md5us:
                status_update("MD5 Hash Error")
                stdout("MD5 Mismatch: hashes differ", True)
                stdout("Ours: %s" % md5us, True)
                stdout("Them: %s" % md5up, True)
                return False
            return True

    #######################
    def install_manaplus():
        status_update("Creating ManaPlus directory...", 61)
        installdir=get_path("manaplus")
        os.mkdir(installdir)
        ## Detect your plataform
        #########################################################
        if renpy.linux:
            if not download_manaplus("ManaPlus.AppImage"):
                return False

            status_update("Marking as executable...", 72)
            execute("chmod +x \"%s\"" % installdir+"/ManaPlus.AppImage", shell=True)
            status_update("Installation successful!", 75)
        #########################################################
        #elif renpy.windows:
        #    if not download_manaplus("ManaPlus.zip"):
        #        return False

        #    status_update("Unzipping file...", 72)
        #    with zipfile.ZipFile(installdir+"/ManaPlus.zip", 'r') as zip_ref:
        #        zip_ref.extractall(installdir)
        #    status_update("Installation successful!", 75)
        #########################################################
        #elif renpy.android:
        #elif renpy.macintosh:
        #elif renpy.emscripten: # web
        #########################################################
        else:
            status_update("ERROR: Unsupported Plataform")
            return False
        return True

    ###############################
    def cli_manaplus(launch=False, download=True):
        global SCR_PROMPT, SCR_RESULT
        ## Check if ManaPlus is already installed
        try:
            MANAPLUS=os.path.exists(get_path("manaplus"))
        except:
            traceback.print_exc()
            MANAPLUS=False

        ## Installer
        if not MANAPLUS and download:
            SCR_PROMPT=("Selected client \"%s\" is not installed.\nDo you wish to install it now?\n\n{size=14}By installing you agree with its {a=%s}Terms of Use and Conditions{/a}.%s{/size}" %
            ("ManaPlus", "https://gitlab.com/manaplus/manaplus/-/raw/master/COPYING",
            ifte(renpy.linux, "\n{i}libfuse2{/i} is required to run AppImages.", "")))
            while SCR_RESULT is None:
                time.sleep(0.02)
            ret=copy.copy(SCR_RESULT)
            SCR_RESULT=None
            if (not ret):
                return False

            try:
                if not install_manaplus():
                    # Delete the failed attempt before raising the exception
                    shutil.rmtree(get_path("manaplus"))
                    #os.rmdir(get_path("manaplus"))
                    raise Exception("Installation failed!")
            except:
                traceback.print_exc()
                stdout("Installation failed!", True)
                return False
        elif not MANAPLUS:
            return False

        ##########
        if launch:
            if renpy.linux:
                os.environ["APPIMAGELAUNCHER_DISABLE"]="1"
                pathy=get_path("manaplus")+"/ManaPlus.AppImage"
                return pathy.replace(" ", "\\ ")
            elif renpy.windows:
                pathy=get_path("manaplus")+"/Mana/manaplus.exe"
                return pathy.replace("/", "\\")
            else:
                stdout("Invalid Plataform!")
                return False
        return True

