#################################################################################
#     This file is part of Mana Launcher.
#     Copyright (C) 2021  Jesusalva <jesusalva@tmw2.org>
#
#     Distributed under the MIT license.
#################################################################################
# Contains definitions and animations

image TMW2 = "images/TMW2.png"
image MLP  = "images/mirrorlake.png"
image DKSD = "images/darkshadow.png"
image DKBG = "#210"
#image Aether = "images/aether.png"

define evil = Character("???", color="#f00")

image aethyr1:
    "aether"
    alpha 0.6
    parallel:
        xalign 0.0
        linear 15.0 xalign 1.0
        linear 15.0 xalign 0.0
        repeat

    parallel:
        yalign 0.0
        linear 15.0 yalign 1.0
        linear 15.0 yalign 0.0
        repeat

transform bgobj:
    alpha 0.7

